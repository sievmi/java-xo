package model;

/**
 * Created by sievmi on 02.04.17.
 */
public class Player {

    private final String name;

    private final Figure figure;

    public Player(final String name, final Figure figure) {
        this.name = name;
        this.figure = figure;
    }

    public String getName() {

        return name;
    }

    public Figure getFigure() {

        return figure;
    }

}

