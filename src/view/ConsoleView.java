package view;

import controllers.MoveController;
import model.Field;
import model.Figure;
import model.Point;
import model.exceptions.AlreadyOccupiedException;
import model.exceptions.InvalidPointException;

import java.util.Scanner;

/**
 * Created by sievmi on 02.04.17.
 */
public class ConsoleView implements View{

    public Point getMoveCoordinate(final Field field) {
        return new Point(0, 0);
    }

    public void show(final Field field) {
        final StringBuilder fieldBuilder = new StringBuilder();
        for (int x = 0; x < field.getSize(); x++) {
            if (x != 0)
                generateSeparator(fieldBuilder);
            generateLine(field, x, fieldBuilder);
        }
        System.out.println(fieldBuilder.toString());
    }

    void generateLine(final Field field,
                      final int x,
                      final StringBuilder sb) {
        try {
            for (int y = 0; y < field.getSize(); y++) {
                if (y != 0)
                    sb.append("|");
                sb.append(" ");
                final Figure figure;
                figure = field.getFigure(new Point(y, x));
                sb.append(figure != null ? figure : " ");
                sb.append(" ");
            }
            sb.append("\n");
        } catch (InvalidPointException e) {
            throw new RuntimeException(e);
        }

    }

    public void askPoint(final MoveController moveController, final Field field, final Figure figure) {
        System.out.println("Current Move:" + figure);
        Scanner in = new Scanner(System.in);
        System.out.println("Enter X: ");
        int x = in.nextInt() - 1;
        System.out.println("Enter Y: ");
        int y = in.nextInt() - 1;
        try {
            moveController.applyFigure(field, new Point(x, y), figure);
        } catch (InvalidPointException | AlreadyOccupiedException e) {
            askPoint(moveController, field, figure);
        }
    }

    void generateSeparator(final StringBuilder sb) {
        sb.append("~~~~~~~~~~~\n");
    }

}

