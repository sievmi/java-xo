package view;

/**
 * Created by sievmi on 06.04.17.
 */

import controllers.MoveController;
import model.Field;
import model.Figure;

public interface View {

    void show(Field field);

    void askPoint(MoveController moveController, Field field, Figure figure);
}
