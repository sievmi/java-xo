package view;

/**
 * Created by sievmi on 02.04.17.
 */
import controllers.CurrentMoveController;
import controllers.MoveController;
import controllers.WinnerController;
import model.Field;
import model.Figure;
import model.Point;
import model.exceptions.AlreadyOccupiedException;
import model.exceptions.InvalidPointException;
import view.View;
import view.ConsoleView;

import java.util.Scanner;

public class Main {

    public static void main(String... args) {
        final Field field = new Field();
        final CurrentMoveController currentMoveController = new CurrentMoveController();
        final MoveController moveController = new MoveController();
        final WinnerController winnerController = new WinnerController();
        View view = new ConsoleView();
        System.out.println("Hello, it's XO game");
        view.show(field);

        Figure figure;
        while((figure = currentMoveController.currentMove(field)) != null) {
            view.askPoint(moveController, field, figure);
            view.show(field);
            if((figure = winnerController.getWinner(field)) != null) {
                System.out.println(figure + " WIN !");
                break;
            }
        }

        System.out.println("GAME END.");
    }

}

