package controllers;

import model.Field;
import model.Figure;
import model.Point;
import model.exceptions.InvalidPointException;
import model.exceptions.AlreadyOccupiedException;

/**
 * Created by sievmi on 02.04.17.
 */
public class MoveController{

    public void applyFigure (final Field field,
                            final Point point,
                            final Figure figure) throws InvalidPointException, AlreadyOccupiedException{
        field.setFigure(point, figure);
    }

}