package controllers;

import model.Field;
import model.Figure;
import model.Point;
import model.exceptions.AlreadyOccupiedException;
import model.exceptions.InvalidPointException;

/**
 * Created by sievmi on 02.04.17.
 */
public class CurrentMoveController {

    public Figure currentMove(final Field field) {
        int countFigure = 0;
        for (int y = 0; y < field.getSize(); y++) {
            for(int x = 0; x < field.getSize(); x++) {
                try {
                    if (field.getFigure(new Point(x, y)) != null)
                        countFigure++;
                } catch (InvalidPointException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        if (countFigure == field.getSize() * field.getSize())
            return null;

        if (countFigure % 2 == 0)
            return Figure.X;
        else
            return Figure.O;

    }

}

