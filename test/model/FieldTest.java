package model;

import model.exceptions.InvalidPointException;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by sievmi on 05.04.17.
 */
public class FieldTest {
    @Test
    public void getSize() throws Exception {

        final Field field = new Field();

        assertEquals(3, field.getSize());

    }

    @Test
    public void setFigure() throws Exception {
        final Field field = new Field();
        final Point inputPoint = new Point(0, 0);
        final Figure inputFigure = Figure.O;

        field.setFigure(inputPoint, inputFigure);
        final Figure actualFigure = field.getFigure(inputPoint);

        assertEquals(inputFigure, actualFigure);
    }

    @Test
    public void getFigureWhenXisLessThenZero() throws Exception {
        final Field field = new Field();
        final Point inputPoint = new Point(-1, 0);

        try {
            field.getFigure(inputPoint);
            fail();
        } catch (final InvalidPointException e) {}
    }

}