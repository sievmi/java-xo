package model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by sievmi on 05.04.17.
 */
public class PlayerTest {
    @Test
    public void getName() throws Exception {

        final String inputValue = "Evgeny";
        final String expectedValue = inputValue;

        final Player player = new Player(inputValue, null);

        final String actualValue = player.getName();

        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void getFigure() throws Exception {
        final Figure inputValue = Figure.X;
        final Figure expectedValue = inputValue;

        final Player player = new Player(null, inputValue);

        final Figure actualValue = player.getFigure();

        assertEquals(expectedValue, actualValue);
    }

}